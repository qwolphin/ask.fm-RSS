import lxml.html
import requests as rq


class InternetException(Exception):
    '''
    Exception for handing errors
    with getting data from web
    '''
    pass


class Dialog:
    '''
    Represents Q&A pair
    '''
    def __init__(self, q, a, date, link):
        '''
        Simple init: self.a = a
        '''
        self.question = q
        self.answer = a
        self.date = date
        self.link = link

class Page:
    '''
    Represents ask.fm user profile
    page using lxml internal form.
    Gives methods for quick extracting
    some data
    '''

    def __init__(self, url):
        '''
        Create dom-like repesintation
        of page at given URL
        '''
        try:
            binary = rq.get(url)
        except Exception:
            raise InternetException("Common internet-related error")
        
        code = binary.status_code
        if code == 200:
            string = binary.text
        elif code == 404:
            raise InternetException("User doesn't exist")
        else:
            raise InternetException("Common internet-related error")
        
        self.page = lxml.html.document_fromstring(string)
       
    def dialogs(self):
        '''
        Returns array of Dialog objects
        representing ask.fm question and answer
        ''' 
        dialogs = []
        pairs = self.page.xpath("//div[@class='item streamItem streamItem-answer']")
        
        for pair in pairs:
            question = pair.xpath("div[@class='streamItemContent streamItemContent-question']/h2")[0]
            question_text = question.text_content()
            answer_array = pair.xpath("div[@class='answerWrapper']/p[@class='streamItemContent streamItemContent-answer']")
            
            if answer_array:
                answer_text = answer_array[0].text_content()
            else:
                answer_text = "_Not_text_answer_"
            
            date_link = pair.xpath("div[@class='streamItemContent streamItemContent-footer']/div[@class='streamItemsAge links']/a")[0]
            date_text = date_link.attrib['title']
            link = 'http://ask.fm' + date_link.attrib['href']
            dialog = Dialog(question_text, answer_text, date_text, link)
            dialogs.append(dialog)
            
        return dialogs

    def title(self):
        '''
        Returns page's title
        '''
        return self.page.xpath('//title')[0].text_content()

def cut_question(text):
    '''
    >>> cut_question('yyep' + ' ' + 'lol' * 70) 
    'yyep'
    
    Cuts string to last space before 60 chars,
    removes punctuation marks at the end
    '''
    if len(text) > 60:
        text = text[:60]
        last_space_index = text.rfind(' ')
        text = text[:last_space_index]
        while text[-1] in ',(.!&:;%#@':
            text = text[:-1]
        text += '...'
    
    return text 
