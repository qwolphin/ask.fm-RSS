#!/usr/bin/python3
from feedgen.feed import FeedGenerator
from bottle import route, run, default_app
from askfm_parser import Page, InternetException, Dialog, cut_question
import argparse

def port(a):
    '''
    Validate input as valid integer between 0 and 65535
    '''
    try:
        port = int(a)
    except ValueError:
        msg = str(a) + " isn't valid integer"
        raise argparse.ArgumentTypeError(msg)
    
    if port > 65535 or port < 0:
        msg = "Port number should be between 0 and 65535"
        raise argparse.ArgumentTypeError(msg)
    else:
        return port

#Create web app on top of Bottle framework
@route('/askfm/<name>')
def askfm(name):
    '''
    Generates rss feed
    '''
    url = 'http://ask.fm/{}'.format(name)
    try:
        dom = Page(url)
    except InternetException as e:
        return str(e)
       
    dialogs = dom.dialogs()
       
    fg = FeedGenerator()
    fg.title('@' + name)
    fg.link(href=url, rel='alternate')
    fg.description('ask.fm RSS from user {}'.format(name))
    fg.author(name=name)
    for d in dialogs:
        fe = fg.add_entry()
        fe.title(cut_question(d.question))
        description = '<b>Question:</b><br>' + d.question + '<br><b>Answer:</b><br>' + d.answer
        fe.description(description)
        fe.pubdate(d.date)
        fe.link(href=d.link, rel='alternate')
        fe.guid(d.link)
       
    return fg.rss_str(pretty=True)

app = default_app()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Local RSS server for extracing data from ask.fm")
    parser.add_argument('--port', type=port, default=8080, help="Port for running server. 8080 is used if argument isn't given")
    args = parser.parse_args()
    run(host='localhost', port=args.port)
