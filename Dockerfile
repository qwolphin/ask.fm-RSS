FROM alpine:3.4 
RUN apk add --update \
    python \
    python-dev \
    py-pip \
    build-base \
    git \
    libxml2 \
    libxml2-dev \
    libxslt \
    libxslt-dev \
    && rm -rf /var/cache/apk/*
RUN pip install bottle gunicorn

WORKDIR /srv/www
RUN git clone https://github.com/wolphin/ask.fm-RSS.git . && \
    pip install -r requirements.txt

EXPOSE 8080
CMD ["gunicorn", "-b 0.0.0.0:8080", "-w 4 ", "askfmrss:app"]
